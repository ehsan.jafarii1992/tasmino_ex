import Data from "@/helper/data";
import Classes from "./style.module.css";
import clsx from "clsx";
import Text from "@/components/atome/Text/text";
import Image from "next/image";
import Mic from "@/public/icon/megaphone.svg";
import { useEffect } from "react";

const Items = ({ selected, setSelected }) => {
  useEffect(() => {
    setSelected(Data?.find((it, idx) => idx == 0 && it));
  }, [Data]);
  return (
    <div className={Classes.itemBox}>
      {Data.map(it => (
        <div
          key={it.id}
          className={clsx(
            it.id === 1
              ? "before:bg-mainRed"
              : it.id === 2
              ? "before:bg-mainYelow"
              : "before:bg-mainBlue",
            selected?.id === it.id ? Classes.active : Classes.item
          )}
          onClick={() => setSelected(it)}
        >
          <div className={Classes.cl} style={{ backgroundColor: it.color }}>
            <Image
              src={Mic}
              alt="mic"
              layout="cover"
              objectFit="cover"
              className="w-[90%] h-[90%]"
            />
          </div>
          <div className={Classes.textBox}>
            <Text
              className={clsx(
                Classes.title,
                selected?.id === it.id
                  ? it.id === 1
                    ? "text-[#F21045]"
                    : it.id === 2
                    ? "text-[#FFA041]"
                    : "text-[#3987FF]"
                  : ""
              )}
            >
              {it.title}
            </Text>
            <Text className={Classes.subTitle}>{it.subTitle}</Text>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Items;

import Text from "@/components/atome/Text/text";
import Classes from "./style.module.css";
import clsx from "clsx";
const Box = ({ selected }) => {
  return (
    <div className={Classes.Box}>
      <Text className={Classes.title}>{selected.title}</Text>
      <div className={Classes.bar}>
        {selected.children?.arr.map(it => (
          <div key={it.id} className="flex flex-row items-center">
            <span
              className={clsx(
                Classes.dotBig,
                selected.id === 1
                  ? "bg-mainRed"
                  : selected.id === 2
                  ? "bg-mainYelow"
                  : "bg-mainBlue"
              )}
            >
              <span className={Classes.dotS}></span>
            </span>
            <Text className={Classes.barTitle}>{it.title}</Text>
          </div>
        ))}
      </div>
      <Text className={Classes.desc}>{selected.children?.description}</Text>
    </div>
  );
};

export default Box;

import Box from "./box";
import Items from "./items";
import Classes from "./style.module.css";
import { useState, useEffect } from "react";

const Services = () => {
  const [selected, setSelected] = useState(1);

  console.log(selected);

  return (
    <div className={Classes.Services}>
      <Items selected={selected} setSelected={setSelected} />
      <Box selected={selected} />
    </div>
  );
};

export default Services;

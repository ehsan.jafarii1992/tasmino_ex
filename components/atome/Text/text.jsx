import React from "react";

const Text = React.forwardRef(({ children, className, ...props }, ref) => {
  return (
    <p ref={ref} className={className} {...props}>
      {children}
    </p>
  );
});

Text.displayName = "Text";

export default Text;

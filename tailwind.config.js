/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}"
  ],
  theme: {
    extend: {
      colors: {
        blueC: "#E1EDFF",
        yelowC: "#FFF0E1",
        redC: "#F6608330",
        secondWhite: "#F7F9Fe",
        mainRed: "#F21045",
        mainYelow: "#FFA041",
        mainBlue: "#3987FF"
      }
    }
  },
  plugins: []
};
